﻿using CreditImmobilier.booklet;
using CreditImmobilier.model;
using CreditImmobilier.view_model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CreditImmobilier.view
{
    /// <summary>
    /// Logique d'interaction pour TP2.xaml
    /// </summary>
    public partial class TP2 : Page
    {
        ViewModelPlacement viewModel;
        public TP2()
        {
            InitializeComponent();
            viewModel = new ViewModelPlacement();
            this.DataContext = viewModel;
            viewModel.TypesAbondement = new List<string>(new string[] { "Mensuel", "Trimestriel", "Annuel" });
            viewModel.TypesPlacements = new List<string>(new string[] { "LivretA", "LDD", "PEL", "Houdini" });
        }

        private void CalculateLivretA_Click(object sender, RoutedEventArgs e)
        {
            Placement placement;
            switch (viewModel.TypePlacementSelectionne)
            {
                case "LivretA":
                    placement = new Placement(0.75, 22950);
                    OuverturePlacement(placement);
                    break;
                case "LDD":
                    placement = new Placement(0.5, 12000);
                    OuverturePlacement(placement);
                    break;
                case "PEL":
                    placement = new Placement(1, 61200);
                    OuverturePlacement(placement);
                    break;
                case "Houdini":
                    placement = new Placement(8, double.MaxValue);
                    OuverturePlacement(placement);
                    break;
                default:
                    placement = new Placement(0, 0);
                    OuverturePlacement(placement);
                    break;
            }
            CalculCapital(placement);
            viewModel.Capital = placement.Capital;
        }

        private void CalculCapital(Placement placement)
        {
            List<LigneResultatPlacement> tableauPlacement = new List<LigneResultatPlacement>();
            for (int i = 0; i < viewModel.DureePlacement; i++)
            {
                tableauPlacement.AddRange(placement.CalculCapital());
            }
            viewModel.ResultatPlacement = tableauPlacement;
        }

        private void OuverturePlacement(Placement placement)
        {
            placement.Capital = viewModel.Capital;
            placement.Abondement = viewModel.Abondement;
            placement.TypeAbondementSelectionne = viewModel.TypeAbondementSelectionne;
        }
        private void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

    }
}
