﻿using CreditImmobilier.view_model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace CreditImmobilier.booklet
{
    public class PlacementAvecLimite: Placement
    {
        PlacementAvecLimite(double taux, double plafond): base(taux, plafond)
        { 
        }
    }
}
