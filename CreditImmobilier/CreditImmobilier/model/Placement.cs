﻿using CreditImmobilier.model;
using CreditImmobilier.view_model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace CreditImmobilier.booklet
{
    public class Placement: ViewModelBase
    {
        protected double _capital;
        protected double _taux;
        protected double _plafond;
        protected double _abondement;
        private string _typeAbondementSelectionne;
        private DateTime _date;

        public Placement(double taux, double plafond)
        {
            this._capital = 0;
            this._taux = taux;
            this._plafond = plafond;
            this._date = new DateTime(2021, 1, 1);
        }

        public string TypeAbondementSelectionne
        {
            get { return _typeAbondementSelectionne; }
            set { _typeAbondementSelectionne = value;}
        }

        public double Capital
        {
            get { return _capital; }
            set { _capital = Math.Round(value, 2, MidpointRounding.ToZero); NotifyPropertyChanged(); }
        }

        public double Abondement
        {
            get { return _abondement; }
            set { _abondement = Math.Round(value, 2, MidpointRounding.ToZero); NotifyPropertyChanged(); }
        }

        public DateTime Date
        {
            get { return _date; }
            set { _date = value; NotifyPropertyChanged(); }
        }

        private double CalculInteretsParQuinzaine()
        {
            if (Capital > _plafond) {
                return (_plafond * (_taux / 100)) / 24;
            }
            return Capital * _taux / 100 / 24;
        }

        public List<LigneResultatPlacement> CalculCapital()
        {
            List<LigneResultatPlacement> tableauPlacement = new List<LigneResultatPlacement>();
            switch (_typeAbondementSelectionne)
            {
                case "Mensuel":
                    CalculCapitalAbondementMensuel(tableauPlacement);
                    break;
                case "Trimestriel":
                    CalculCapitalAbondementTrimestriel(tableauPlacement);
                    break;
                case "Annuel":
                    CalculCapitalAbondementAnnuel(tableauPlacement);
                    break;
                default:
                    break;
            }
            this.Date = new DateTime(this.Date.Year + 1, 1, 1);
            return tableauPlacement;
        }

        private void CalculCapitalAbondementMensuel(List<LigneResultatPlacement> tableauPlacement)
        {
            double cumulInterets = 0;
            double interets = 0;
            for (int i = 0; i < 12; i++)
            {
                AjoutAbondement();
                for (int j = 0; j < 2; j++)
                {
                    interets = CalculInteretsParQuinzaine();
                    cumulInterets += interets;
                    AjouteLigneParQuinzaine(tableauPlacement, interets, cumulInterets);
                }
            }
            Capital += cumulInterets;
        }

        private void CalculCapitalAbondementTrimestriel(List<LigneResultatPlacement> tableauPlacement)
        {
            double cumulInterets = 0;
            double interets = 0;
            for (int i = 0; i < 4; i++)
            {
                AjoutAbondement();
                for (int j = 0; j < 6; j++)
                {
                    interets = CalculInteretsParQuinzaine();
                    cumulInterets += interets;
                    AjouteLigneParQuinzaine(tableauPlacement, interets, cumulInterets);
                }
            }
            Capital += cumulInterets;
        }

        private void CalculCapitalAbondementAnnuel(List<LigneResultatPlacement> tableauPlacement)
        {
            AjoutAbondement();
            double cumulInterets = 0;
            double interets = 0;
            for (int i = 0; i < 24; i++)
            {
                interets = CalculInteretsParQuinzaine();
                cumulInterets += interets;
                AjouteLigneParQuinzaine(tableauPlacement, interets, cumulInterets);
            }
            Capital += cumulInterets;
        }

        public virtual void AjoutAbondement()
        {
            if (_capital + _abondement < _plafond)
            {
                Capital += _abondement;
            }
        }
        private void AjouteLigneParQuinzaine(List<LigneResultatPlacement> tableauPlacement, double interets, double cumulInterets)
        {
            LigneResultatPlacement ligneTableauPlacement = new LigneResultatPlacement
            {
                Annee = this.Date,
                Capital = this.Capital,
                Taux = this._taux,
                Interets = interets,
                CumulInterets = cumulInterets
            };
            tableauPlacement.Add(ligneTableauPlacement);
        }
    }
}
