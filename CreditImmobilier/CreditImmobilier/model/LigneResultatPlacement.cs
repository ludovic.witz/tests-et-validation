﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmobilier.model
{
    public class LigneResultatPlacement
    {
        public DateTime _annee;
        public double _capital;
        public double _taux;
        public double _interets;
        public double _cumulInterets;

        public LigneResultatPlacement()
        {
            Capital = 0;
            Interets = 0;
            CumulInterets = 0;
        }

        public DateTime Annee
        {
            get { return _annee; }
            set { _annee = value; }
        }

        public double Capital
        {
            get { return _capital; }
            set { _capital = Math.Round(value, 2, MidpointRounding.ToZero); }
        }
        public double Taux
        {
            get { return _taux; }
            set { _taux = value; }
        }
        public double Interets
        {
            get { return _interets; }
            set { _interets = Math.Round(value, 2, MidpointRounding.ToZero); }
        }
        public double CumulInterets
        {
            get { return _cumulInterets; }
            set { _cumulInterets = Math.Round(value, 2, MidpointRounding.ToZero); }
        }
    }
}
