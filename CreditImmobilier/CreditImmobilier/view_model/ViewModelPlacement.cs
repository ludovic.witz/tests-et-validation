﻿using CreditImmobilier.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmobilier.view_model
{
    class ViewModelPlacement: ViewModelBase
    {
        private List<string> _typesAbondemment;
        private string _typeAbondementSelectionne = "Annuel";
        private List<string> _typesPlacements;
        private string _typePlacementSelectionne = "LivretA";
        private double _capital = 0;
        private double _abondement = 0;
        private int _dureePlacement = 1;
        private List<LigneResultatPlacement> _resultatPlacement = new List<LigneResultatPlacement>();

        public List<string> TypesAbondement
        {
            get { return _typesAbondemment; }
            set { _typesAbondemment = value; }
        }

        public string TypeAbondementSelectionne
        {
            get { return _typeAbondementSelectionne; }
            set { _typeAbondementSelectionne = value; }
        }

        public List<string> TypesPlacements
        {
            get { return _typesPlacements; }
            set { _typesPlacements = value; }
        }

        public string TypePlacementSelectionne
        {
            get { return _typePlacementSelectionne; }
            set { _typePlacementSelectionne = value; }
        }

        public double Capital
        {
            get { return _capital; }
            set { _capital = Math.Round(value, 2, MidpointRounding.ToZero); NotifyPropertyChanged(); }
        }

        public double Abondement
        {
            get { return _abondement; }
            set { _abondement = Math.Round(value, 2, MidpointRounding.ToZero); NotifyPropertyChanged(); }
        }

        public int DureePlacement
        {
            get { return _dureePlacement; }
            set { _dureePlacement = value; }
        }

        public List<LigneResultatPlacement> ResultatPlacement
        {
            get { return _resultatPlacement; }
            set { _resultatPlacement = value; NotifyPropertyChanged(); }
        }

    }
}
