using System;
using Xunit;
using CreditImmobilier.booklet;

namespace XUnitTestProjectCreditImmobilier
{
    public class UnitTestLivretA
    {
        [Fact]
        public void TestAbondementAnnuel()
        {
            Placement livretA = new Placement(0.75, 22950);
            livretA.TypeAbondementSelectionne = "Annuel";
            livretA.Capital = 0;
            livretA.Abondement = 100;
            livretA.CalculCapital();
            Assert.Equal(100.75, livretA.Capital);
        }
    }
}
