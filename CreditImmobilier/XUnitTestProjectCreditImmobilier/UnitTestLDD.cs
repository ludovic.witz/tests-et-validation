using System;
using Xunit;
using CreditImmobilier.booklet;

namespace XUnitTestProjectCreditImmobilier
{
    public class UnitTestLDD
    {
        [Theory]
        [InlineData(100, 100.5)]
        [InlineData(200, 201)]
        [InlineData(500,502.5)]
        public void TestAbondementAnnuel(int abondement, double capitalCalcule)
        {
            Placement ldd = new Placement(0.5, 12000);
            ldd.TypeAbondementSelectionne = "Annuel";
            ldd.Capital = 0;
            ldd.Abondement = abondement;
            ldd.CalculCapital();
            Assert.Equal(capitalCalcule, ldd.Capital);
        }
    }
}
