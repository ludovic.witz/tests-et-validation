using System;
using Xunit;
using JeuDeBowling;

namespace XUnitTestJeuDeBowling
{
    public class UnitTest1
    {
        [Fact]
        public void DebutDuJeu()
        {
            
            Joueur joueur = new Joueur();
            PartieBowling partie = new PartieBowling(joueur);
            Assert.Equal(0, joueur.Score);
        }

        [Theory]
        [InlineData(1, 10)]
        [InlineData(2, 30)]
        [InlineData(3, 60)]
        [InlineData(4, 90)]
        [InlineData(5, 120)]
        [InlineData(6, 150)]
        [InlineData(7, 180)]
        [InlineData(8, 210)]
        [InlineData(9, 240)]
        [InlineData(10, 270)]
        [InlineData(11, 290)]
        [InlineData(12, 300)]
        public void JoueurFaitStrikes(int nbStrike, int score)
        {
            Joueur joueur = new Joueur();
            PartieBowling partie = new PartieBowling(joueur);
            for (int i = 0; i < nbStrike; i++)
            {
                partie.RenverseQuille(10);
            }
            Assert.Equal(score, joueur.Score);
        }

        [Theory]
        [InlineData(7, 0, 7)]
        public void JoueurLanceBoules(int nbQuillePremierLancer, int nbQuilleDeuxiemeLancer, int score)
        {
            Joueur joueur = new Joueur();
            PartieBowling partie = new PartieBowling(joueur);
            partie.RenverseQuille(nbQuillePremierLancer);
            partie.RenverseQuille(nbQuilleDeuxiemeLancer);
            Assert.Equal(score, joueur.Score);
        }

        [Fact]
        public void JoueurFaitPlusieursLancers()
        {
            Joueur joueur = new Joueur();
            PartieBowling partie = new PartieBowling(joueur);
            partie.RenverseQuille(7);
            partie.RenverseQuille(0);
            partie.RenverseQuille(9);
            partie.RenverseQuille(1);
            partie.RenverseQuille(4);
            partie.RenverseQuille(1);
            Assert.Equal(26, joueur.Score);
        }

        [Theory]
        [InlineData(new int[] { 4, 0, 10, 10, 0, 5, 8, 2, 4, 0, 7, 0 }, 69)]
        [InlineData(new int[] { 7, 0, 9, 1, 4, 1, 5, 5, 0, 10, 10, 10, 10, 5, 1, 1, 4}, 138)]
        [InlineData(new int[] { 7, 0, 9, 1, 4, 1, 5, 5, 0, 10, 10, 10, 10, 5, 1, 1, 9, 5}, 148)]
        [InlineData(new int[] { 7, 0, 9, 1, 4, 1, 5, 5, 0, 10, 10, 10, 10, 5, 1, 1, 9, 10}, 153)]
        public void Score(int[] points, int score)
        {
            Joueur joueur = new Joueur();
            PartieBowling partie = new PartieBowling(joueur);
            foreach (int point in points)
            {
                partie.RenverseQuille(point);
            }
            Assert.Equal(score, joueur.Score);
        }

        [Theory]
        [InlineData(new int[] { 7, 0, 9, 1, 4, 1, 5, 5, 0, 10, 10, 10, 10, 5, 1, 1, 4})]
        [InlineData(new int[] { 7, 0, 9, 1, 4, 1, 5, 5, 0, 10, 10, 10, 10, 5, 1, 1, 9, 5})]
        [InlineData(new int[] { 7, 0, 9, 1, 4, 1, 5, 5, 0, 10, 10, 10, 10, 5, 1, 1, 9, 10})]
        [InlineData(new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1})]
        public void FinPartie(int[] points)
        {
            Joueur joueur = new Joueur();
            PartieBowling partie = new PartieBowling(joueur);
            foreach (int point in points)
            {
                partie.RenverseQuille(point);
            }
            Assert.True(partie.EstTermine());
        }

        [Fact]
        public void NombreTropGrand()
        {
            Joueur joueur = new Joueur();
            PartieBowling partie = new PartieBowling(joueur);
            Assert.Throws<Exception>(() => partie.RenverseQuille(11));
        }

        [Fact]
        public void NombreTropPetit()
        {
            Joueur joueur = new Joueur();
            PartieBowling partie = new PartieBowling(joueur);
            Assert.Throws<Exception>(() => partie.RenverseQuille(-1));
        }
    }
}
