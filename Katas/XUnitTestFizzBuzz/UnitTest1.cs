using System;
using Xunit;
using FizzBuzz;

namespace XUnitTestFizzBuzz
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz", 15)]
        [InlineData("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz1617Fizz19BuzzFizz2223FizzBuzz26Fizz2829FizzBuzz", 30)]
        [InlineData("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz1617Fizz19BuzzFizz2223FizzBuzz26Fizz2829FizzBuzz3132Fizz34BuzzFizz3738FizzBuzz41Fizz4344FizzBuzz4647Fizz49Buzz", 50)]
        public void TestResult(string result, int n)
        {
            FizzBuzzGenerateur generateur = new FizzBuzzGenerateur();
            Assert.Equal(result, generateur.Generer(n));
        }

        [Fact]
        public void TestMinValeurN()
        {
            FizzBuzzGenerateur generateur = new FizzBuzzGenerateur();
            Assert.Throws<Exception>(() => generateur.Generer(14));
        }

        [Fact]
        public void TestMaxValeurN()
        {
            FizzBuzzGenerateur generateur = new FizzBuzzGenerateur();
            Assert.Throws<Exception>(() => generateur.Generer(151));
        }

        [Fact]
        public void TestReturnString()
        {
            FizzBuzzGenerateur generateur = new FizzBuzzGenerateur();
            Assert.IsType<string>(generateur.Generer(15));
        }
    }
}
