﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeTennis
{
    public class Joueur
    {
        private int _score;
        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        private bool _avantage;
        public bool Avantage
        {
            get { return _avantage; }
            set { _avantage = value; }
        }

        public Joueur()
        {
            this.Score = 0;
            this.Avantage = false;
        }
    }
}
