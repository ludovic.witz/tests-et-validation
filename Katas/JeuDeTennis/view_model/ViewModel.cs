﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeTennis
{
    class ViewModel: ViewModelBase
    {
        private int _scoreJoueur1;
        public int ScoreJoueur1
        {
            get { return _scoreJoueur1; }
            set { _scoreJoueur1 = value;
                NotifyPropertyChanged();
            }
        }

        private int _scoreJoueur2;
        public int ScoreJoueur2
        {
            get { return _scoreJoueur2; }
            set
            {
                _scoreJoueur2 = value;
                NotifyPropertyChanged();
            }
        }

        private bool _avantageJoueur1;
        public bool AvantageJoueur1
        {
            get { return _avantageJoueur1; }
            set { _avantageJoueur1 = value;
                NotifyPropertyChanged(); 
            }
        }

        private bool _avantageJoueur2;
        public bool AvantageJoueur2
        {
            get { return _avantageJoueur2; }
            set
            {
                _avantageJoueur2 = value;
                NotifyPropertyChanged();
            }
        }

    }
}
