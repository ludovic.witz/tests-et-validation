﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JeuDeTennis
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel viewModel;
        Joueur joueur1;
        Joueur joueur2;
        PartieTennis partie;
        public MainWindow()
        {
            InitializeComponent();
            viewModel = new ViewModel();
            this.DataContext = viewModel;
            joueur1 = new Joueur();
            joueur2 = new Joueur();
            partie = new PartieTennis(joueur1, joueur2);
            RefreshView();
        }

        private void RefreshView()
        {
            viewModel.ScoreJoueur1 = joueur1.Score;
            viewModel.ScoreJoueur2 = joueur2.Score;
            viewModel.AvantageJoueur1 = joueur1.Avantage;
            viewModel.AvantageJoueur2 = joueur2.Avantage;
        }

        private void Joueur1Gagne(object sender, RoutedEventArgs e)
        {
            if (partie.GagneEchange(joueur1) != null)
            {
                labelGagnant.Content = "Joueur 1 a gagné !!";
                return;
            }
            RefreshView();
        }

        private void Joueur2Gagne(object sender, RoutedEventArgs e)
        {
            if (partie.GagneEchange(joueur2) != null)
            {
                labelGagnant.Content = "Joueur 2 a gagné !!";
                return;
            }
            RefreshView();
        }
    }
}
