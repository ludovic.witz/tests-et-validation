﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeTennis
{
    public class PartieTennis
    {
        private Joueur _joueur1;
        private Joueur _joueur2;
        public PartieTennis()
        {

        }

        public PartieTennis(Joueur joueur1, Joueur joueur2)
        {
            this._joueur1 = joueur1;
            this._joueur2 = joueur2;
        }

        public Joueur Joueur1
        {
            get { return _joueur1; }
            set { _joueur1 = value; }
        }

        public Joueur Joueur2
        {
            get { return _joueur2; }
            set { _joueur2 = value; }
        }

        public Joueur GagneEchange(Joueur joueur)
        {
            switch (joueur.Score)
            {
                case 0:
                case 15:
                    joueur.Score += 15;
                    break;
                case 30:
                    joueur.Score += 10;
                    break;
                case 40:
                    return TestSiJoueurGagneJeu(joueur);
                default:
                    break;
            }
            return null;
        }

        private void ChangeAvantage(Joueur joueur)
        {
            if (joueur == _joueur1)
            {
                if (_joueur2.Avantage)
                {
                    _joueur2.Avantage = false;
                }
                else
                {
                    _joueur1.Avantage = true;
                }
            }
            else
            {
                if (_joueur1.Avantage)
                {
                    _joueur1.Avantage = false;
                }
                else
                {
                    _joueur2.Avantage = true;
                }
            }
        }

        private Joueur TestSiJoueurGagneJeu(Joueur joueur)
        {
            if (_joueur1.Score == 40 && _joueur2.Score != 40)
            {
                return _joueur1;
            }
            if (_joueur2.Score == 40 && _joueur1.Score != 40)
            {
                return _joueur2;
            }
            else
            {
                if (joueur.Avantage == true)
                {
                    return joueur;
                }
                else
                {
                    ChangeAvantage(joueur);
                }
            }
            return null;
        }
    }
}
