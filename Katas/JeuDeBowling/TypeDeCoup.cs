﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeBowling
{
    enum Coup
    {
        Simple,
        Trou,
        Spare,
        Strike,
    }

    public class TypeDeCoup
    {
        private Enum _coup;
        public Enum Coup
        {
            get { return _coup; }
            set { _coup = value; }
        }
        private int _value;
        public int Value
        {
            get { return _value; }
            set { _value = value; }
        }
        public TypeDeCoup(Enum coup, int value)
        {
            this._coup = coup;
            this._value = value;
        }
    }
}
