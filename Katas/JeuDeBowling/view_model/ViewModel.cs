﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeBowling
{
    class ViewModel: ViewModelBase
    {
        private int _score;
        public int Score
        {
            get { return _score; }
            set { _score = value;
                NotifyPropertyChanged();
            }
        }

        private int _scoreFrame;
        public int ScoreFrame
        {
            get { return _scoreFrame; }
            set {
                _scoreFrame = value;
                NotifyPropertyChanged(); 
            }
        }

        private List<string> _coups = new List<string>(new string[] { "X", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"});
        public List<string> Coups
        {
            get { return _coups; }
            set { _coups = value; }
        }

        private string _coupSelectionne;
        public string CoupSelectionne
        {
            get { return _coupSelectionne; }
            set { _coupSelectionne = value; }
        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set { _message = value; 
                NotifyPropertyChanged(); }
        }
    }
}
