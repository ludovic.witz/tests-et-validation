﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace JeuDeBowling
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel viewModel;
        Joueur joueur;
        PartieBowling partie;
        public MainWindow()
        {
            InitializeComponent();
            viewModel = new ViewModel();
            this.DataContext = viewModel;
            joueur  = new Joueur();
            partie = new PartieBowling(joueur);
            viewModel.Score = joueur.Score;
        }

        private void buttonLancer_Click(object sender, RoutedEventArgs e)
        {
            switch (viewModel.CoupSelectionne)
            {
                case "X":
                    partie.RenverseQuille(10);
                    break;
                default:
                    int coup = 0;
                    try
                    {
                        coup = int.Parse(viewModel.CoupSelectionne);
                    }
                    catch { }
                    partie.RenverseQuille(coup);
                    break;
            }

            if (partie.EstTermine())
            {
                viewModel.Message = "Partie Terminée";
            }
            viewModel.Score = joueur.Score;
        }
    }
}
