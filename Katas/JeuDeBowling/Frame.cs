﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeBowling
{
    public class Frame
    {
        private int _numero;
        public int Numero
        {
            get { return _numero; }
            set { _numero = value; }
        }
        private int _score; 
        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }
        private List<TypeDeCoup> _coups = new List<TypeDeCoup>();
        public List<TypeDeCoup> Coups
        {
            get { return _coups; }
            set { _coups = value; }
        }

        public Frame() { }
        public Frame(int numero)
        {
            _numero = numero;
        }
    }
}
