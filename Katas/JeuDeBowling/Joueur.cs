﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeBowling
{
    public class Joueur
    {
        private int _score;
        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        public Joueur()
        {
            this.Score = 0;
        }
    }
}
