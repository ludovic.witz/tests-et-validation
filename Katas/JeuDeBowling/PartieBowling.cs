﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeuDeBowling
{
    public class PartieBowling
    {
        private Joueur _joueur;
        private int _nbQuillesDebouts;
        private List<Frame> _frames = new List<Frame>();
        public Joueur Joueur
        {
            get { return _joueur; }
            set { _joueur = value; }
        }

        public int NbQuillesDebouts
        {
            get { return _nbQuillesDebouts; }
            set { _nbQuillesDebouts = value; }
        }
        public List<Frame> Frames
        {
            get { return _frames; }
            set { _frames = value; }
        }

        public PartieBowling() { }
        public PartieBowling(Joueur joueur)
        {
            _joueur = joueur;
            _nbQuillesDebouts = 10;
            _frames.Add(new Frame(1));
        }

        public void RenverseQuille(int nbQuilles)
        {
            VerifieNombreDeQuilles(nbQuilles);
            if (_frames.Last().Numero >= 10)
            {
                JoueDerniereFrame(nbQuilles);
            }
            else
            {
                VerifieAvantDernierCoup(nbQuilles);
                VerifieDernierCoup(nbQuilles);
                if (nbQuilles == 10 && _frames.Last().Coups.Count == 0)
                {
                    JoueurFaitStrike();
                }
                else
                {
                    if (_frames.Last().Coups.Count == 0)
                    {
                        _nbQuillesDebouts -= nbQuilles;
                        _frames.Last().Coups.Add(new TypeDeCoup(Coup.Simple, nbQuilles));
                        return;
                    }
                    else
                    {
                        _nbQuillesDebouts -= nbQuilles;
                        if (_nbQuillesDebouts == 0)
                        {
                            JoueurFaitSpare(10 - _nbQuillesDebouts);
                        }
                        else
                        {
                            JoueurFaitTrou(10 - _nbQuillesDebouts);
                        }
                    }
                    _nbQuillesDebouts = 10;
                }
                _frames.Add(new Frame(_frames.Count + 1));
            }
        }

        private void JoueDerniereFrame(int nbQuilles)
        {
            if (EstTermine()) 
            {
                return;
            }
            if (nbQuilles == 10)
            {
                if (_frames.Last().Coups.Count == 0)
                {
                    VerifieAvantDernierCoup(nbQuilles);
                    VerifieDernierCoup(nbQuilles);
                    JoueurFaitStrike();
                }
                else if (_frames.Last().Coups.Count == 1)
                {
                    VerifieDernierCoup(nbQuilles);
                    JoueurFaitStrike();

                }
                else if (_frames.Last().Coups.Count == 2)
                {
                    JoueurFaitStrike();
                }
            }
            else
            {
                if (_frames.Last().Coups.Count == 0)
                {
                    VerifieAvantDernierCoup(nbQuilles);
                    VerifieDernierCoup(nbQuilles);
                    _nbQuillesDebouts -= nbQuilles;
                    _frames.Last().Coups.Add(new TypeDeCoup(Coup.Simple, nbQuilles));
                    return;
                }
                else if (_frames.Last().Coups.Count == 1)
                {
                    VerifieDernierCoup(nbQuilles);
                    _nbQuillesDebouts -= nbQuilles;
                    if (_nbQuillesDebouts == 0)
                    {
                        JoueurFaitSpare(10 - _nbQuillesDebouts);
                    }
                    else
                    {
                        JoueurFaitTrou(10 - _nbQuillesDebouts);
                    }
                }
                else if (_frames.Last().Coups.Count == 2)
                {
                    _joueur.Score += nbQuilles;
                }
            }
        }

        private void JoueurFaitStrike()
        {
            _joueur.Score += 10;
            _frames.Last().Coups.Add(new TypeDeCoup(Coup.Strike, 10));
            _frames.Last().Score = _joueur.Score;
        }

        private void JoueurFaitSpare(int score)
        {
            _joueur.Score += 10;
            _frames.Last().Coups.Add(new TypeDeCoup(Coup.Spare, score));
            _frames.Last().Score = _joueur.Score;
        }

        private void JoueurFaitTrou(int score)
        {
            _joueur.Score += score;
            _frames.Last().Coups.Add(new TypeDeCoup(Coup.Trou, score));
            _frames.Last().Score = _joueur.Score;
        }

        private void VerifieAvantDernierCoup(int nbQuilles)
        {
            if (_frames.Count > 1 && Coup.Strike.Equals(_frames[^2].Coups.Last().Coup))
            {
                if (_frames.Count > 2 && Coup.Strike.Equals(_frames[^3].Coups.Last().Coup))
                {
                    _joueur.Score += nbQuilles;
                }
            }
        }

        private void VerifieDernierCoup(int nbQuilles)
        {
            if (_frames.Count > 1)
            {
                if (_frames.Last().Coups.Count == 0)
                {
                    if (Coup.Strike.Equals(_frames[^2].Coups.Last().Coup) || Coup.Spare.Equals(_frames[^2].Coups.Last().Coup))
                    {
                        _joueur.Score += nbQuilles;
                    }
                }
                else
                {
                    if (Coup.Strike.Equals(_frames[^1].Coups.Last().Coup) || Coup.Spare.Equals(_frames[^1].Coups.Last().Coup))
                    {
                        _joueur.Score += nbQuilles;
                    }
                }
            }
        }

        private void VerifieNombreDeQuilles(int nbQuilles)
        {
            if (nbQuilles < 0 || nbQuilles > 10)
            {
                throw new Exception("Le nombre de quilles n'est pas entre 0 et 10.");
            }
        }

        public bool EstTermine()
        {
            return (_frames.Last().Coups.Count == 3 || (_frames.Last().Coups.Count == 2 && (Coup.Simple.Equals(_frames.Last().Coups.Last().Coup) || Coup.Trou.Equals(_frames.Last().Coups.Last().Coup))));
        }
    }
}
