﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    class ViewModel: ViewModelBase
    {
        private int _nombre;
        private string _resultat;

        public int Nombre
        {
            get { return _nombre; }
            set { _nombre = value; }
        }

        public string Resultat
        {
            get { return _resultat; }
            set { _resultat = value;
                NotifyPropertyChanged(); 
            }
        }

    }
}
