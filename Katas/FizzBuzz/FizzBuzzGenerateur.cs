﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz
{
    public class FizzBuzzGenerateur
    {
        public FizzBuzzGenerateur()
        {

        }

        public string Generer(int n)
        {
            if (n < 15 || n > 150) {
                throw new Exception("Le nombre n'est pas entre 15 et 150.");
            }

            string result = "";

            for (int i = 1; i <= n; i++)
            {
                result = DetermineMultiple(result, i);
            }

            return result;
        }

        private static string DetermineMultiple(string result, int i)
        {
            if ((i % 3 == 0) && (i % 5 == 0))
            {
                result += "FizzBuzz";
            }
            else if (i % 3 == 0)
            {
                result += "Fizz";
            }
            else if (i % 5 == 0)
            {
                result += "Buzz";
            }
            else
            {
                result += i;
            }

            return result;
        }
    }
}
