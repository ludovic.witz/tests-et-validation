using System;
using Xunit;
using JeuDeTennis;

namespace XUnitTestJeuDeTennis
{
    public class UnitTest1
    {
        [Fact]
        public void DebutDuJeu()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            PartieTennis partie = new PartieTennis(joueur1, joueur2);
            Assert.Equal(0, joueur1.Score);
            Assert.Equal(0, partie.Joueur1.Score);
        }

        [Fact]
        public void Joueur1GagneUnEchangeScore()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            PartieTennis partie = new PartieTennis(joueur1, joueur2);
            partie.GagneEchange(joueur1);
            Assert.Equal(15, joueur1.Score);
            Assert.Equal(15, partie.Joueur1.Score);
        }

        [Fact]
        public void Joueur1GagneDeuxEchangesScore()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            PartieTennis partie = new PartieTennis(joueur1, joueur2);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur1);
            Assert.Equal(30, joueur1.Score);
            Assert.Equal(30, partie.Joueur1.Score);
        }

        [Fact]
        public void Joueur1Joueur2GagneUnEchangeScore()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            PartieTennis partie = new PartieTennis(joueur1, joueur2);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur2);
            Assert.Equal(15, joueur1.Score);
            Assert.Equal(15, partie.Joueur1.Score);
            Assert.Equal(15, joueur2.Score);
            Assert.Equal(15, partie.Joueur2.Score);
        }

        [Fact]
        public void Joueur1Joueur2GagneDeuxEchangesScore()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            PartieTennis partie = new PartieTennis(joueur1, joueur2);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur2);
            Assert.Equal(30, joueur1.Score);
            Assert.Equal(30, partie.Joueur1.Score);
            Assert.Equal(30, joueur2.Score);
            Assert.Equal(30, partie.Joueur2.Score);
        }

        [Fact]
        public void Joueur1GagneJeu()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            PartieTennis partie = new PartieTennis(joueur1, joueur2);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur1);
            Assert.Equal(joueur1, partie.GagneEchange(joueur1));
        }

        [Fact]
        public void Joueur2GagneJeu()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            PartieTennis partie = new PartieTennis(joueur1, joueur2);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur2);
            Assert.Equal(joueur2, partie.GagneEchange(joueur2));
        }

        [Fact]
        public void Joueur1GagneJeuApresEgalite()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            PartieTennis partie = new PartieTennis(joueur1, joueur2);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur1);
            Assert.Equal(joueur1, partie.GagneEchange(joueur1));
        }

        [Fact]
        public void Joueur2GagneJeuApresEgalite()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            PartieTennis partie = new PartieTennis(joueur1, joueur2);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur2);
            Assert.Equal(joueur2, partie.GagneEchange(joueur2));
        }

        [Fact]
        public void Joueur1GagneJeuApresEgaliteAvantage()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            PartieTennis partie = new PartieTennis(joueur1, joueur2);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur1);
            Assert.True(joueur1.Avantage);
        }

        [Fact]
        public void Joueur1EgaliteApresEgalite()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            PartieTennis partie = new PartieTennis(joueur1, joueur2);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur1);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur2);
            partie.GagneEchange(joueur2);
            Assert.Null(partie.GagneEchange(joueur1));
        }
    }
}
